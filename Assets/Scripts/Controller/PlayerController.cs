﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour {

    public Health health;

    public Joystick joystick;
    public float runSpeed;

    public GameObject arrowPrefab;
    public Transform shootingPoint;
    public Rigidbody2D shootingPointRb;

    public float shootingRange = 50f;
    public float delayBetweenShoots = 2f;

    private float horizontalMove;
    private float verticalMove;

    private float blockShootTime = 0f;

    private LayerMask enemyLayer;
    private LayerMask flyEnemyLayer;
    private LayerMask obstacleLayer;
    private LayerMask finishLayer;

    private Rigidbody2D rb;

    [HideInInspector] public UnityEvent onDeath = new UnityEvent();


    void Start() {
        rb = GetComponent<Rigidbody2D>();

        enemyLayer = LayerMask.GetMask("Enemy");
        flyEnemyLayer = LayerMask.GetMask("FlyingEnemy");
        obstacleLayer = LayerMask.GetMask("Obstacle");
        finishLayer = LayerMask.GetMask("Finish");

        health.onChange.AddListener(OnHealthChange);
        health.Init();
    }

    void Update() {
        if(!GameManager.Instance.IsActive)
            return;

        if(blockShootTime > 0f) {
            blockShootTime -= Time.deltaTime;
            return;
        }

        if(joystick.Horizontal != 0f || joystick.Vertical != 0f) {
            return;
        }

        ShootingLogic();
    }

    void FixedUpdate() {
        if(!GameManager.Instance.IsActive)
            return;

        horizontalMove = joystick.Horizontal * runSpeed;
        verticalMove = joystick.Vertical * runSpeed;

        rb.velocity = new Vector2(horizontalMove, verticalMove);
    }


    public void TakeDamage(int dmg) {
        health.TakeDamage(dmg);
    }


    private void ShootingLogic() {
        Collider2D[] enemies1 = Physics2D.OverlapCircleAll(transform.position, shootingRange, enemyLayer);
        Collider2D[] enemies2 = Physics2D.OverlapCircleAll(transform.position, shootingRange, flyEnemyLayer);

        Collider2D[] enemies = new Collider2D[enemies1.Length + enemies2.Length];

        for (int i = 0; i < enemies1.Length; i++) {
            enemies[i] = enemies1[i];
        }

        for (int j = 0; j < enemies2.Length; j++) {
            enemies[enemies1.Length + j] = enemies2[j];
        }

        if (enemies.Length > 0) {
            Transform nearestEnemy = enemies[0].transform;
            float nearestDistanceSqr = Mathf.Infinity;
            foreach (Collider2D enemyCollider in enemies) {
                float distanceSqr = (enemyCollider.transform.position - transform.position).sqrMagnitude;
                if (distanceSqr < nearestDistanceSqr) {
                    nearestDistanceSqr = distanceSqr;
                    nearestEnemy = IsObstacleOnAttackWay(enemyCollider.transform) ? nearestEnemy : enemyCollider.transform;
                }
            }

            if (nearestEnemy != null) {
                Vector3 direction = nearestEnemy.position - shootingPoint.position;
                shootingPoint.up = direction.normalized;

                ShootArrow(direction.normalized);
            }
        }
    }

    private void ShootArrow(Vector3 direction) {
        GameObject arrow = Instantiate(arrowPrefab, shootingPoint.position, Quaternion.identity);
        ArrowController arrowController = arrow.GetComponent<ArrowController>();
        if (arrowController != null) {
            arrowController.Shoot(direction);
        }
        blockShootTime += delayBetweenShoots;
    }


    private void OnHealthChange(int health) {
        if(health <= 0)
            onDeath.Invoke();
    }


    private bool IsObstacleOnAttackWay(Transform enemy) {
        Vector3 directionToEnemy = (enemy.position - transform.position).normalized;
        float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, directionToEnemy, distanceToEnemy, obstacleLayer);
		if(hit.collider != null) {
			return true;
		}
        return false;
    }


    private void OnTriggerEnter2D(Collider2D other) {
        if ((finishLayer.value & (1 << other.gameObject.layer)) != 0) {
            if(CheckWinCondition())
                GameManager.Instance.GameOver();
        }
    }

    private bool CheckWinCondition() {
        Collider2D[] enemies = Physics2D.OverlapCircleAll(transform.position, shootingRange, enemyLayer);
        return enemies.Length == 0;
    }
}