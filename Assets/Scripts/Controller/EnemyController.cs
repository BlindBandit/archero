using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    public Health health;

    public Transform shootingPoint;

    public GameObject arrowPrefab;

    public int collisionDamage;
    public float shootingRange = 10f;
    public float delayBetweenShoots = 2f;
    public float moveSpeed = 10f;
    public int rewardValue;

    private float blockShootTime = 0f;

    private LayerMask playerLayer;
    private LayerMask obstacleLayer;

    protected PlayerController player;


    void Start() {
        playerLayer = LayerMask.GetMask("Player");
        obstacleLayer = LayerMask.GetMask("Obstacle");

        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        health.onChange.AddListener(OnHealthChange);
    }

    void Update() {
        if(!GameManager.Instance.IsActive)
            return;
            
        float distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
        if(distanceToPlayer <= shootingRange) {
            if(blockShootTime > 0f) {
                blockShootTime -= Time.deltaTime;
                return;
            }
            Vector3 direction = player.transform.position - shootingPoint.position;
            shootingPoint.up = direction.normalized;
            ShootArrow(direction.normalized);
        }
        else {
            MoveTowardsPlayerWithObstacleAvoidance();
        }
    }


    public virtual void Init() {
        health.Init();
    }


    protected virtual void MoveTowardsPlayerWithObstacleAvoidance() {
        Vector3 directionToPlayer = (player.transform.position - transform.position).normalized;

        Vector3 potentialDirection = directionToPlayer;

        Collider2D[] obstacles = Physics2D.OverlapCircleAll(transform.position, 2f, obstacleLayer);
        foreach (Collider2D obstacle in obstacles) {
            Vector3 obstacleDirection = (obstacle.transform.position - transform.position).normalized;
            float distance = Vector3.Distance(transform.position, obstacle.transform.position);
            float obstacleForce = 1f / Mathf.Pow(distance, 2f);
            potentialDirection -= obstacleForce * obstacleDirection;
        }

        potentialDirection.Normalize();

        transform.position += potentialDirection * moveSpeed * Time.deltaTime;
    }


    private void ShootArrow(Vector3 direction) {
        GameObject arrow = Instantiate(arrowPrefab, shootingPoint.position, Quaternion.identity);
        ArrowController arrowController = arrow.GetComponent<ArrowController>();
        if (arrowController != null) {
            arrowController.Shoot(direction);
        }
        blockShootTime += delayBetweenShoots;
    }


    private void OnHealthChange(int health) {
        if(health <= 0) {
            Destroy(gameObject);
            UserData.Instance.AddGold(rewardValue);
        }
    }


    private void OnTriggerEnter2D(Collider2D other) {
        if ((playerLayer.value & (1 << other.gameObject.layer)) != 0) {
            player.TakeDamage(collisionDamage);
        }
    }
}
