using UnityEngine;

public class ArrowController : MonoBehaviour {
    public float arrowSpeed = 10f;
    public bool isEnemy;
    public int damage;

    private LayerMask enemyLayer;
    private LayerMask flyEnemyLayer;
    private LayerMask obstacleLayer; 

    private Vector3 direction;


    void Start() {
        enemyLayer = isEnemy ? LayerMask.GetMask("Player") : LayerMask.GetMask("Enemy");
        flyEnemyLayer  = isEnemy ? LayerMask.GetMask("Player") : LayerMask.GetMask("FlyingEnemy");
        obstacleLayer = LayerMask.GetMask("Obstacle");
    }

    private void Update() {
        MoveArrow();
    }


    public void Shoot(Vector3 shootDirection) {
        direction = shootDirection;
        LookInDirection(direction);
    }

    private void MoveArrow() {
        transform.Translate(direction * arrowSpeed * Time.deltaTime, Space.World);
    }


    private void OnTriggerEnter2D(Collider2D other) {
        if ((obstacleLayer.value & (1 << other.gameObject.layer)) != 0) {
            DestroyArrow();
        }

        if ((enemyLayer.value & (1 << other.gameObject.layer)) != 0) {
            DealDamage(other.gameObject);
            DestroyArrow();
        }

        if ((flyEnemyLayer.value & (1 << other.gameObject.layer)) != 0) {
            DealDamage(other.gameObject);
            DestroyArrow();
        }
    }


    private void LookInDirection(Vector3 targetDirection) {
        float angle = Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg - 90f;;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }


    private void DestroyArrow() {
        Destroy(gameObject);
    }

    private void DealDamage(GameObject target) {
        target.GetComponent<Health>().TakeDamage(damage);
    }
}