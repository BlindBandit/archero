using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour {
    public int maxHealth = 100;
	public int currentHealth;

    public bool isDead => currentHealth == 0;

    [HideInInspector] public UnityEvent<int> onChange = new UnityEvent<int>();
    [HideInInspector] public UnityEvent<int> onSetMax = new UnityEvent<int>();


    public void Init() {
        currentHealth = maxHealth;
        onSetMax.Invoke(currentHealth);
    }


	public void TakeDamage(int damage) {
		currentHealth = Mathf.Max(0 , currentHealth - damage);
		onChange.Invoke(currentHealth);
	}
}
