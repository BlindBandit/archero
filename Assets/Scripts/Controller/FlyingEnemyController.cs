using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemyController : EnemyController {
    public override void Init() {
        base.Init();

        /*int ignoreCollisionLayer = LayerMask.NameToLayer("Obstacle");
        Collider2D[] collidersToIgnore = Physics2D.OverlapCollider(GetComponent<Collider2D>(), new ContactFilter2D(), ignoreCollisionLayer);

        foreach (Collider2D collider in collidersToIgnore) {
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), collider, true);
        }*/
    }

    protected override void MoveTowardsPlayerWithObstacleAvoidance() {
        Vector3 directionToPlayer = (player.transform.position - transform.position).normalized;
        transform.position += directionToPlayer * moveSpeed * Time.deltaTime;
    }
}
