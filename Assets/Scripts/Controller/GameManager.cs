using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour{
	
	public GameplayState State;

	public bool IsActive => State == GameplayState.Play;
	public bool IsPause => State == GameplayState.Pause;

	public GameObject gameOver;

    public UICountdown countdown;

	public static GameManager Instance { get; private set; }

	void Awake() {
		Instance = this;
		State = GameplayState.Pause;
	}

    void Start() {
        //LevelManager.Instance.onLevelInited.AddListener(StartCountdown);

        PlayerController player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        player.onDeath.AddListener(GameOver);

        StartCountdown();
    }


	public void Play() {
		State = GameplayState.Play;
	}

	public void Pause(bool isPause) {
		State = isPause ? GameplayState.Pause : GameplayState.Play;
	}

	public void GameOver() {
		State = GameplayState.Pause;
        gameOver.SetActive(true);
	}

	public void PlayAgain() {
		UnityEngine.SceneManagement.SceneManager.LoadScene(0);
	}


    private void StartCountdown() {
        StartCoroutine(Countdown(3));
    }

    private IEnumerator Countdown(int number) {
        countdown.gameObject.SetActive(true);
        countdown.Set(number);

        yield return new WaitForSeconds(1f);

        if(number > 1)
            StartCoroutine(Countdown(number-1));
        else {
            countdown.gameObject.SetActive(false);
            Play();
        }
    }
}

public enum GameplayState {Play, Pause}
