using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.Events;

public class LevelManager : MonoBehaviour {

    public List<GameObject> enemies;

    public int enemySpawn_X_Min, enemySpawn_X_Max;
    public int enemySpawn_Y_Min, enemySpawn_Y_Max;

    public int enemiesCount;

    public float checkSpawnRadius;

    [HideInInspector] public UnityEvent onLevelInited = new UnityEvent();

    private LayerMask obstacleLayer;

    public static LevelManager Instance { get; private set; }


    void Awake() {
		Instance = this;
    }

    void Start() {
        obstacleLayer = LayerMask.GetMask("Obstacle");

        Init();
    }

    
    public void Init() {
        for(int i = 0; i < enemiesCount; i++) {
            float x = (float) Random.Range(enemySpawn_X_Min, enemySpawn_X_Max);
            float y = (float) Random.Range(enemySpawn_Y_Min, enemySpawn_Y_Max);
            Vector3 spawnPoint = new Vector3(x, y, transform.position.z);
            bool isValidSpawnPoint = CheckValidSpawnPoint(spawnPoint);

            if (isValidSpawnPoint) {
                GameObject enemy = Instantiate(enemies[Random.Range(0, enemies.Count)], spawnPoint, Quaternion.identity);
                EnemyController enemyController = enemy.GetComponent<EnemyController>();
                enemyController.Init();
            }
            else
                i--;
        }

        onLevelInited.Invoke();
    }


    private bool CheckValidSpawnPoint(Vector3 spawnPosition) {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(spawnPosition, checkSpawnRadius, obstacleLayer);
        return colliders.Length == 0;
    }
}
