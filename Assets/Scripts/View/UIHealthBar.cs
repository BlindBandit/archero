using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHealthBar : MonoBehaviour {
    public Health health;

	public Slider slider;
	public Image fill;


    void Awake() {
        health.onSetMax.AddListener(SetMaxHealth);
        health.onChange.AddListener(SetHealth);
    }

	public void SetMaxHealth(int health) {
		slider.maxValue = health;
		slider.value = health;
	}

    public void SetHealth(int health) {
		slider.value = health;
	}
}
