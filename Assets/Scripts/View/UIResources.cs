using UnityEngine;
using TMPro;

public class UIResources : MonoBehaviour {
    public TMP_Text gold;

    void Start() {
        UserData.Instance.onChange.AddListener(Refresh);
    }

    void Refresh() {
        gold.text = "Gold: " + UserData.Instance.Gold.ToString();
    }
}
