using UnityEngine;
using UnityEngine.UI;

public class UIGameOver : MonoBehaviour {
    public Button restarnBtn;

    void Start() {
        restarnBtn.onClick.AddListener(OnBtnRestartClick);
    }

    private void OnBtnRestartClick() {
        GameManager.Instance.PlayAgain();
    }
}
