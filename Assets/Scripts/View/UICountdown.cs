using UnityEngine;
using TMPro;

public class UICountdown : MonoBehaviour {
    public TMP_Text numberText;

    public void Set(int number) {
        numberText.text = number.ToString();
    }
}
