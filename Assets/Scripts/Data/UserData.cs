using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class UserData : MonoBehaviour {
    public int Gold;

    [HideInInspector] public UnityEvent onChange = new UnityEvent();

    public static UserData Instance { get; private set; }

	void Awake() {
		Instance = this;

        Gold = 0;
	}

    public void AddGold(int value) {
        Gold += value;

        onChange.Invoke();
    }
}
